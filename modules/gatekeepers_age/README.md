# Gatekeepers - Age Gate

- [Introduction](#introduction)
- [Purpose](#purpose)
- [Structure](#structure)
- [How To Use](#how-to-use)
- [Examples](#examples)
- [Requirements](#requirements)

## Introduction

Welcome to the Age Gate module!

## Purpose

Prompts visitors for their location and age to ensure they are legally allowed to visit the site.

## Structure

Coming soon!

## How To Use

Coming soon!

## Examples

Coming soon!

## Requirements

PHP minimal version required: **7.2**
