<?php

namespace Drupal\gatekeepers_age\Constants;

/**
 * Defines constants for the Age Gate cookie.
 */
final class AgeGateCookie {

  /**
   * Age Gate cookie ID.
   *
   * @var string
   */
  public const ID = 'gatekeepers_age';

  /**
   * Age Gate cookie authorization property.
   */
  public const PROPERTY_AUTHORIZED = 'authorized';

}
