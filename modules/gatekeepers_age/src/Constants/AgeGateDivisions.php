<?php

namespace Drupal\gatekeepers_age\Constants;

/**
 * Defines constants for Age Gate divisions.
 */
final class AgeGateDivisions {

  /**
   * Divisions ID.
   */
  public const ID = 'divisions';

  /**
   * Divisions label.
   */
  public const LABEL = 'Divisions';

  /**
   * Division label (singular).
   */
  public const LABEL_SINGLE = 'Division';

  /**
   * Division name property.
   */
  public const PROPERTY_NAME = 'name';

  /**
   * Division overrides property.
   */
  public const PROPERTY_OVERRIDES = 'overrides';

  /**
   * Division override property.
   */
  public const PROPERTY_OVERRIDE = 'override';

  /**
   * Division age property.
   */
  public const PROPERTY_AGE = 'age';

  /**
   * Division divisions property.
   */
  public const PROPERTY_DIVISIONS = 'divisions';

}
