<?php

namespace Drupal\gatekeepers_age\Constants;

/**
 * Defines constants for the Age Gate JavaScript properties.
 */
final class AgeGateJs {

  /**
   * Age Gate JavaScript ID.
   *
   * @var string
   */
  public const ID = 'age';

}
