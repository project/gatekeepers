<?php

namespace Drupal\gatekeepers_age\Constants;

/**
 * Defines constants for Age Gate labels.
 */
final class AgeGateLabels {

  /**
   * Labels ID.
   */
  public const ID = 'labels';

  /**
   * Labels label.
   */
  public const LABEL = 'Labels';

  /**
   * Label label (singular).
   */
  public const LABEL_SINGLE = 'Label';

  /**
   * Label name property.
   */
  public const PROPERTY_NAME = 'name';

  /**
   * Label text property.
   */
  public const PROPERTY_TEXT = 'text';

}
