<?php

namespace Drupal\gatekeepers_age\Constants;

/**
 * Defines constants for the Age Gate settings.
 */
final class AgeGateSettings {

  /**
   * Config Form ID for the Age Gate settings.
   *
   * @var string
   */
  public const FORM_ID = 'gatekeepers_age_form';

  /**
   * Age Gate settings configuration ID.
   *
   * @var string
   */
  public const CONFIG_ID = 'gatekeepers.age.settings';

}
