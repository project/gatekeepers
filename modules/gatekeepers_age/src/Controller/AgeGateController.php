<?php

namespace Drupal\gatekeepers_age\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\gatekeepers_age\Constants\AgeGateDivisions;
use Drupal\gatekeepers_age\Constants\AgeGateLabels;
use Drupal\gatekeepers_age\Constants\AgeGateSettings;
use Drupal\gatekeepers_age\Utility\AgeGateTree;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class AgeGateController.
 *
 * @noinspection PhpUnused
 */
class AgeGateController extends ControllerBase {

  /**
   * Get the Age Gate configuration values.
   *
   * @noinspection PhpUnused
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The retrieved Age Gate configuration values.
   */
  public function getAge() : JsonResponse {
    $data = [];

    // Append necessary data.
    $this->appendLabels($data);
    $this->appendDivisions($data);

    return new JsonResponse($data);
  }

  /**
   * Append the labels data to the provided data array.
   *
   * @param array $data
   *   The provided data array.
   */
  private function appendLabels(array &$data) : void {
    $labels = $this->config(AgeGateSettings::CONFIG_ID)->get(AgeGateLabels::ID);
    $data[AgeGateLabels::ID] = $labels;
  }

  /**
   * Append the age data to the provided data array.
   *
   * @param array $data
   *   The provided data array.
   */
  private function appendDivisions(array &$data) : void {
    $divisions = $this->config(AgeGateSettings::CONFIG_ID)->get(AgeGateDivisions::ID);
    AgeGateTree::fillBlanks($divisions);
    $data[AgeGateDivisions::ID] = $divisions;
  }

}
