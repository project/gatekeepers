<?php

namespace Drupal\gatekeepers_age\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\State;
use Drupal\gatekeepers_age\Constants\AgeGateDivisions;
use Drupal\gatekeepers_age\Constants\AgeGateLabels;
use Drupal\gatekeepers_age\Constants\AgeGateSettings;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Age Gate settings.
 *
 * @noinspection PhpUnused
 */
final class AgeGateSettingsForm extends ConfigFormBase {

  /**
   * Provides the state system using a key value store.
   *
   * @var \Drupal\Core\State\State
   */
  protected $state;

  /**
   * AgeGateSettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The Drupal Configuration Factory Service.
   * @param \Drupal\Core\State\State $state
   *   The Drupal State Storage Service.
   *   Provides the state system using a key value store.
   */
  public function __construct(ConfigFactoryInterface $configFactory, State $state) {
    parent::__construct($configFactory);
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $container->get('config.factory'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return AgeGateSettings::FORM_ID;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() : array {
    return [AgeGateSettings::CONFIG_ID];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) : array {
    // Format form as tree for proper values nesting.
    $form['#tree'] = TRUE;

    // Attach relevant sub-forms to the main Age Gate form.
    $config = $this->config(AgeGateSettings::CONFIG_ID);
    $this->attachLabelsForm($form, $config->get(AgeGateLabels::ID));
    $this->attachDivisionsForm($form, $config->get(AgeGateDivisions::ID));

    // Parent function call.
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) : void {
    // Retrieve and clean up submitted values.
    $labels = $form_state->getValues()[AgeGateLabels::ID];
    $divisions = $form_state->getValues()[AgeGateDivisions::ID];
    $this->removeDivisionsOverrides($divisions);

    // Save values to the configuration.
    $this->configFactory->getEditable(AgeGateSettings::CONFIG_ID)
      ->set(AgeGateLabels::ID, $labels)
      ->set(AgeGateDivisions::ID, $divisions)
      ->save();

    // Parent function call.
    parent::submitForm($form, $form_state);
  }

  /**
   * Add Form fields allowing labels to be customized.
   *
   * @param array $form
   *   Form to attach new elements to.
   * @param array $labels
   *   The current labels.
   */
  private function attachLabelsForm(array &$form, array $labels) : void {
    $this->buildLabelsWrapper($form);
    $form[AgeGateLabels::ID] = $this->buildLabels($labels, $form[AgeGateLabels::ID]);
  }

  /**
   * Add Form fields allowing age requirement to be overridden by divisions.
   *
   * Divisions are abstract, and can be nested recursively. Our top-level
   * divisions are currently Countries, but we might need to specify Planets in
   * the future!
   *
   * @param array $form
   *   Form to attach new elements to.
   * @param array $divisions
   *   The current divisions.
   */
  private function attachDivisionsForm(array &$form, array $divisions) : void {
    $this->buildDivisionsWrapper($form);
    $form[AgeGateDivisions::ID] = $this->buildDivisions($divisions, $form[AgeGateDivisions::ID]);
  }

  /**
   * Builds the wrapper render array element for all labels.
   *
   * @param array $form
   *   Form to attach new elements to.
   */
  private function buildLabelsWrapper(array &$form) : void {
    $form[AgeGateLabels::ID] = [
      '#type' => 'details',
      '#title' => $this->t('@name', ['@name' => AgeGateLabels::LABEL]),
      '#description' => $this->t('Configure @id settings.', ['@id' => AgeGateLabels::ID]),
      '#open' => TRUE,
    ];
  }

  /**
   * Builds the wrapper render array element for all divisions.
   *
   * @param array $form
   *   Form to attach new elements to.
   */
  private function buildDivisionsWrapper(array &$form) : void {
    $form[AgeGateDivisions::ID] = [
      '#type' => 'details',
      '#title' => $this->t('@name', ['@name' => AgeGateDivisions::LABEL]),
      '#description' => $this->t('Configure @id settings.', ['@id' => AgeGateDivisions::ID]),
      '#open' => TRUE,
    ];
  }

  /**
   * Build the render array elements for the provided labels.
   *
   * @param array $labels
   *   The provided labels.
   * @param array $context
   *   The provided render array context.
   *
   * @return array
   *   The render array elements for the provided labels.
   */
  private function buildLabels(array $labels, array $context) : array {
    foreach ($labels as $id => $properties) {
      // Build label render array elements.
      $context[$id][AgeGateLabels::PROPERTY_NAME] = $this->buildLabelHiddenName($properties);
      $context[$id][AgeGateLabels::PROPERTY_TEXT] = $this->buildLabelTextfield($properties);
    }

    return $context;
  }

  /**
   * Build the render array elements for the provided divisions and context.
   *
   * @param array $divisions
   *   The provided divisions.
   * @param array $context
   *   The provided render array context.
   *
   * @return array
   *   The render array elements for the provided divisions.
   */
  private function buildDivisions(array $divisions, array $context) : array {
    foreach ($divisions as $id => $properties) {
      // Build division render array elements.
      $context[$id] = $this->buildDivisionElements($id, $properties);

      // Build division render array sub-elements.
      if (!empty($properties[AgeGateDivisions::PROPERTY_DIVISIONS])) {
        $context[$id][AgeGateDivisions::PROPERTY_DIVISIONS] = $this->buildDivisionSubElements($properties);
      }
    }

    return $context;
  }

  /**
   * Build division render array elements.
   *
   * @param string $id
   *   The current division ID.
   * @param array $properties
   *   The current division properties.
   *
   * @return array
   *   The division render array elements.
   */
  private function buildDivisionElements(string $id, array $properties) : array {
    $elements = [];

    // Prepare states properties.
    $override = AgeGateDivisions::PROPERTY_OVERRIDE;
    $wrapperStates = ['visible' => [":input[name$='[{$id}_{$override}]']" => ['checked' => TRUE]]];

    // Add base elements.
    $elements[$id] = $this->buildDivisionWrapper($properties, $wrapperStates);
    $elements[$id][AgeGateDivisions::PROPERTY_NAME] = $this->buildDivisionHiddenName($properties);
    $elements[$id][AgeGateDivisions::PROPERTY_AGE] = $this->buildDivisionAgeRequirement($properties);

    // Add override elements if sub-elements are present.
    if (!empty($properties[AgeGateDivisions::PROPERTY_DIVISIONS])) {
      $elements[$id][AgeGateDivisions::PROPERTY_OVERRIDES] = $this->buildDivisionAgeOverrides($properties);
    }

    return $elements[$id];
  }

  /**
   * Build a wrapper render array element based on supplied properties.
   *
   * @param array $properties
   *   The supplied properties.
   * @param array $states
   *   Optional render array element states.
   *
   * @return array
   *   The wrapper render array element.
   */
  private function buildDivisionWrapper(array $properties, array $states = []) : array {
    $name = !empty($properties[AgeGateDivisions::PROPERTY_NAME]) ? $properties[AgeGateDivisions::PROPERTY_NAME] : AgeGateDivisions::LABEL_SINGLE;
    $hasSubElements = !empty($properties[AgeGateDivisions::PROPERTY_DIVISIONS]);
    $type = $hasSubElements ? 'details' : 'fieldset';

    $wrapper = [
      '#type' => $type,
      '#title' => $this->t('@name', ['@name' => $name]),
      '#open' => $this->isAgeOverriddenDivision($properties),
      '#states' => $states,
    ];

    return $wrapper;
  }

  /**
   * Build a hidden name render array element based on supplied properties.
   *
   * @param array $properties
   *   The supplied properties.
   *
   * @return array
   *   The hidden name render array element.
   */
  private function buildLabelHiddenName(array $properties) : array {
    $name = !empty($properties[AgeGateLabels::PROPERTY_NAME]) ? $properties[AgeGateLabels::PROPERTY_NAME] : AgeGateLabels::LABEL_SINGLE;
    return [
      '#type' => 'hidden',
      '#value' => $name,
    ];
  }

  /**
   * Build a textfield render array element based on supplied properties.
   *
   * @param array $properties
   *   The supplied properties.
   *
   * @return array
   *   The textfield render array element.
   */
  private function buildLabelTextfield(array $properties) : array {
    $name = !empty($properties[AgeGateLabels::PROPERTY_NAME]) ? $properties[AgeGateLabels::PROPERTY_NAME] : AgeGateLabels::LABEL_SINGLE;
    $text = !empty($properties[AgeGateLabels::PROPERTY_TEXT]) ? $properties[AgeGateLabels::PROPERTY_TEXT] : NULL;

    $element = [
      '#type' => 'textfield',
      '#title' => $this->t('@name', ['@name' => $name]),
      '#default_value' => $text,
    ];

    return $element;
  }

  /**
   * Build a hidden name render array element based on supplied properties.
   *
   * @param array $properties
   *   The supplied properties.
   *
   * @return array
   *   The hidden name render array element.
   */
  private function buildDivisionHiddenName(array $properties) : array {
    $name = !empty($properties[AgeGateDivisions::PROPERTY_NAME]) ? $properties[AgeGateDivisions::PROPERTY_NAME] : AgeGateDivisions::LABEL_SINGLE;
    return [
      '#type' => 'hidden',
      '#value' => $name,
    ];
  }

  /**
   * Build an age requirement render array element based on supplied properties.
   *
   * @param array $properties
   *   The supplied properties.
   *
   * @return array
   *   The age requirement render array element.
   */
  private function buildDivisionAgeRequirement(array $properties) : array {
    $name = !empty($properties[AgeGateDivisions::PROPERTY_NAME]) ? $properties[AgeGateDivisions::PROPERTY_NAME] : AgeGateDivisions::LABEL_SINGLE;
    $age = !empty($properties[AgeGateDivisions::PROPERTY_AGE]) ? $properties[AgeGateDivisions::PROPERTY_AGE] : NULL;
    $hasSubElements = !empty($properties[AgeGateDivisions::PROPERTY_DIVISIONS]);
    $description = $hasSubElements
      ? $this->t('Set the default required age for @name. This value can be overridden by more specific entries below.', ['@name' => $name])
      : $this->t('Set the default required age for @name.', ['@name' => $name]);

    $element = [
      '#type' => 'number',
      '#title' => $this->t('Required age for @name', ['@name' => $name]),
      '#description' => $description,
      '#default_value' => $age,
      '#min' => 0,
      '#step' => 1,
    ];

    return $element;
  }

  /**
   * Build age override render array elements based on supplied properties.
   *
   * @param array $properties
   *   The supplied division properties.
   *
   * @return array
   *   The age override render array elements.
   */
  private function buildDivisionAgeOverrides(array $properties) : array {
    $elements = [];
    $divisions = !empty($properties[AgeGateDivisions::PROPERTY_DIVISIONS]) ? $properties[AgeGateDivisions::PROPERTY_DIVISIONS] : [];

    foreach ($divisions as $divisionId => $divisionProperties) {
      $name = !empty($divisionProperties[AgeGateDivisions::PROPERTY_NAME]) ? $divisionProperties[AgeGateDivisions::PROPERTY_NAME] : AgeGateDivisions::LABEL_SINGLE;
      $override = AgeGateDivisions::PROPERTY_OVERRIDE;
      $elements["{$divisionId}_{$override}"] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Override for @name', ['@name' => $name]),
        '#default_value' => $this->isAgeOverriddenDivision($divisionProperties),
      ];
    }

    return $elements;
  }

  /**
   * Build division render array sub-elements.
   *
   * @param array $properties
   *   The current division properties.
   *
   * @return array
   *   The division render array sub-elements.
   */
  private function buildDivisionSubElements(array $properties) : array {
    $elements = [];

    $subElements = $properties[AgeGateDivisions::PROPERTY_DIVISIONS];
    $elements[AgeGateDivisions::PROPERTY_DIVISIONS] = [];
    $elements[AgeGateDivisions::PROPERTY_DIVISIONS] = $this->buildDivisions($subElements, $elements[AgeGateDivisions::PROPERTY_DIVISIONS]);

    return $elements[AgeGateDivisions::PROPERTY_DIVISIONS];
  }

  /**
   * Determine whether or not a division possesses an age override.
   *
   * Note that this will parse all division sub-divisions to determine whether
   * or not it is overridden.
   *
   * @param array $properties
   *   The supplied division properties.
   *
   * @return bool
   *   Whether or not a division possesses an age override.
   */
  private function isAgeOverriddenDivision(array $properties) : bool {
    // Check direct property.
    if (!empty($properties[AgeGateDivisions::PROPERTY_AGE])) {
      return TRUE;
    }

    // Check sub-divisions properties.
    if (!empty($properties[AgeGateDivisions::PROPERTY_DIVISIONS])) {
      $divisions = $properties[AgeGateDivisions::PROPERTY_DIVISIONS];
      foreach ($divisions as $division) {
        if ($this->isAgeOverriddenDivision($division)) {
          return TRUE;
        }
      }
    }

    return FALSE;
  }

  /**
   * Remove all overrides key:value entries from the supplied array.
   *
   * Note that this will parse all values sub-divisions recursively.
   *
   * @param array $values
   *   The supplied array from which overrides key:value entries will be
   *   removed.
   */
  private function removeDivisionsOverrides(array &$values) : void {
    // Loop through all values entries.
    foreach ($values as &$value) {
      // Remove direct property.
      unset($value[AgeGateDivisions::PROPERTY_OVERRIDES]);

      // Check sub-divisions properties.
      if (!empty($value[AgeGateDivisions::PROPERTY_DIVISIONS])) {
        $this->removeDivisionsOverrides($value[AgeGateDivisions::PROPERTY_DIVISIONS]);
      }
    }
  }

}
