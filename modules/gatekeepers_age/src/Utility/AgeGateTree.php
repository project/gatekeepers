<?php

namespace Drupal\gatekeepers_age\Utility;

use Drupal\gatekeepers_age\Constants\AgeGateDivisions;

/**
 * Class AgeGateTree.
 *
 * Contains utility methods available to the Age Gate module.
 */
final class AgeGateTree {

  /**
   * Fill blank age values with its closest non-blank parent value.
   *
   * The first iteration should always contain the topmost non-blank default
   * value.
   *
   * @param array $values
   *   The supplied values.
   * @param int $parent
   *   Optional non-blank parent value.
   */
  public static function fillBlanks(array &$values, int $parent = 0) : void {
    foreach ($values as &$properties) {
      // Fill blank age value.
      if (empty($properties[AgeGateDivisions::PROPERTY_AGE])) {
        $properties[AgeGateDivisions::PROPERTY_AGE] = (string) $parent;
      }

      // Recurse into sub-divisions.
      if (!empty($properties[AgeGateDivisions::PROPERTY_DIVISIONS])) {
        self::fillBlanks($properties[AgeGateDivisions::PROPERTY_DIVISIONS], $properties[AgeGateDivisions::PROPERTY_AGE]);
      }
    }
  }

}
