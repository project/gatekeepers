<?php

namespace Drupal\gatekeepers\Constants;

/**
 * Defines constants for the Gatekeepers JavaScript properties.
 */
final class GatekeepersJs {

  /**
   * Gatekeepers JavaScript ID.
   *
   * @var string
   */
  public const ID = 'gatekeepers';

}
