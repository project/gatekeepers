<?php

namespace Drupal\gatekeepers\Constants;

/**
 * Defines constants for the Gatekeepers settings.
 */
final class GatekeepersSettings {

  /**
   * Config Form ID for the Gatekeepers settings.
   *
   * @var string
   */
  public const FORM_ID = 'gatekeepers_form';

  /**
   * Gatekeepers settings configuration ID.
   *
   * @var string
   */
  public const CONFIG_ID = 'gatekeepers.settings';

}
