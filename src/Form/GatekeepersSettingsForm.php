<?php

namespace Drupal\gatekeepers\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\gatekeepers\Constants\GatekeepersSettings;

/**
 * Configure Gatekeepers settings.
 *
 * @noinspection PhpUnused
 */
final class GatekeepersSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return GatekeepersSettings::FORM_ID;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() : array {
    return [GatekeepersSettings::CONFIG_ID];
  }

}
